var
	m,n:longint;
	t,t1:text;
	ind,i,j:integer;

function perfect(k:longint):boolean;
var sum,i:longint;
begin
	//����� ����� �� ���������
	perfect:=false;
	sum:=0;
	i:=1;
	//��������� ���� ��� ������� ������ �����, �� ����� �� ����
	while i<k do begin
		if (k mod i)=0 then sum:=sum+i;
		inc(i);
	end;
	//���� ���� ���� ���� ������ �����, �� �� ��������� �����
	if sum=k then perfect:=true;
end;

begin
	assign(t,'input.txt');
	assign(t1,'output.txt');
	reset(t);
	rewrite(t1);
	readln(t,m,n);
	
	ind:=0;

	for i:=m to n do
		if perfect(i) then begin writeln(t1,i); ind:=1; end;
	
	if ind=0 then writeln(t1,'Absent');
	close(t);
	close(t1);
end.
