program expr;
var inp,out:text;
	a1:array[1..24] of longint;
	a2:array[1..50] of integer;
	s,w:longint;
	i,n,k,j,e:integer;
	t,ts:string;
	t1:array[1..24] of string;
begin
	assign(inp,'expr.in');
	assign(out,'expr.out');
	reset(inp);
	rewrite(out);

	readln(inp,k,s);

	for i:=1 to k do 
		read(inp,a1[i]);
	
	for i:=1 to 24 do
		a2[i]:=1;

	n:=0;
	w:=0;
	{n- вказує скільки від'ємних знаків буде в масиві (в біжучій групі)}
	while n<=k do begin
		{Рухаємо групу відємних знаків по масиву}
		for j:=1 to k do begin
			{Біжучу групу знаків змінюємо на -}
			for i:=j to j+n do begin
				a2[i]:=-1;
			end;
			{Підраховуємо суму елементів масиву з біжучими знаками}
			for i:=1 to k do
				w:=w+a1[i]*a2[i];
			{Якщо задана сума s дорівнює отриманій сумі w, то записуємо результат у вихідний файл}
			if w=s then begin 
				{Для цього перетворюємо кожне число масиву у рядкову змінну}
				for e:=1 to k do begin
					str(a1[e]*a2[e],t1[e]);
					{Всім додатнім числам крім першого додаємо знак + в початок рядка}
					if (e<>1) and (a2[e]>0) then t1[e]:='+'+t1[e];
					{Об'єднуємо увесь приклад}
					t:=t+t1[e];
				end;
				{Добавляємо до отриманого прикладу вираз =сумі і виходимо із програми}
				str(s,ts);
				t:=t+'='+ts;	
				writeln(out,t);
				close(out); 
				close(inp);
				exit; 
			end;
			{Обнуляємо утворену суму і заповнюємо масив знаків одиничками}
			w:=0;
			for i:=1 to k do
				a2[i]:=1;
		end;
		n:=n+1;
	end;
writeln(out,'No solution');
close(inp);
close(out);
end.
