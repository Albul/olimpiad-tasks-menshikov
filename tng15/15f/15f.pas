type tt=array[1..200] of integer;
var
	t,t1:text;
	s1,s2,s,s3:string;
	a1,a2,b1,b2,c1,c2,d1,d2:tt;
	i,j,k,l,pk,ps,err,v1,v2,e,ka,kb,kc,kd:integer;

{ka - Кількість одночленів у многочлені а}

procedure vvod_polinom(s1:string; var a1,a2:tt; var k:integer);
	var i,j,v1,e,pk,ps,err:integer;
		s:string;
begin
	pk:=1;
	k:=0;
	e:=0;
	i:=1;
	v1:=1;

	while i<=length(s1) do begin
		if ((s1[i]='+') or (s1[i]='-') ) and (e=1) then begin 
			k:=k+1;
			s:=copy(s1,v1,i-v1);
			val(s,a1[k],err);
			a2[k]:=0;
			e:=0;
		end;

		if (i=length(s1)) and (e=1) then begin
			k:=k+1;
			s:=copy(s1,v1,i-v1+1);
			val(s,a1[k],err);
			a2[k]:=0;
			e:=0;
		end;

		if (s1[i]='+') or (s1[i]='-') then begin 
				v1:=i;
				e:=1;
		end;
		
		if s1[i]='x' then begin 
			e:=0;
			k:=k+1;			
			s:=copy(s1,pk,i-pk);
			val(s,a1[k],err);

			if a1[k]=0 then a1[k]:=1;

			if (s1[i+1]='+') or (s1[i+1]='-') or (i=length(s1)) then begin 
				pk:=i+2;
				a2[k]:=1;
			end;

			if s1[i+1]='^' then begin
				ps:=i+2;
				for j:=i+2 to length(s1) do begin
					if (s1[j]='+') or (s1[j]='-') then begin 
						s:=copy(s1,ps,j-ps);
						val(s,a2[k],err);
						pk:=j;
						break;
					end;
					if (j=length(s1)) then begin
						s:=copy(s1,ps,j-ps+1);
						val(s,a2[k],err);
						pk:=0; {}
						break;
					end;
				end;
			end;
				
		end;
	
		i:=i+1;
	end;

end;

procedure mnozh_polinom(a1,a2,b1,b2:tt; ka,kb:integer; var c1,c2:tt; var kc:integer);
	var i,j,k:integer;
begin
	kc:=0;

	for i:=1 to ka do
		for j:=1 to kb do begin
			kc:=kc+1;			
			c1[kc]:=a1[i]*b1[j];
			c2[kc]:=a2[i]+b2[j];
	end;

end;

procedure norm_polinom(a1,a2:tt; ka:integer; var b1,b2:tt; var kb:integer);
var i,j,max,maxi,pokaznyk,ind,koef:integer;
begin
	max:=0;
	maxi:=0;
	ind:=0;
	koef:=0;
	kb:=0;	
	for i:=1 to ka do
		if a2[i]>max then begin max:=a2[i]; maxi:=i; end;
	
	pokaznyk:=max;
	ind:=1;

	while pokaznyk>=0 do begin
		if ind=1 then begin
			for i:=1 to ka do
				if a2[i]=pokaznyk then koef:=koef+a1[i];
			kb:=kb+1;							
			b1[kb]:=koef;
			b2[kb]:=pokaznyk;
			ind:=0;
			koef:=0;
			
		end;

		pokaznyk:=pokaznyk-1;
		for i:=1 to ka do
			if a2[i]=pokaznyk then ind:=1;

	end;

end;

procedure vyvod_polinom(a1,a2:tt; ka:integer; var s:string);
	var i,j,k:integer;
		ss:string;
begin
	for i:=1 to ka do begin

		if (a1[i]<>0) and (a2[i]=0) then begin 
			str(a1[i],ss);			
			if a1[i]>0 then s:=s+'+'+ss;
			if a1[i]<0 then s:=s+ss;
		end;

		if (a1[i]=1) and (a2[i]>1) then begin
			s:=s+'x^'; 
			str(a2[i],ss); 
			s:=s+ss;
		end;

		if (a1[i]>1) and (a2[i]=1) and (i<>1) then begin
			str(a1[i],ss); 

			s:=s+'+'+ss+'x'; 
		end;

		if (a1[i]<1) and (a2[i]=1) then begin
			str(a1[i],ss); 
			s:=s+ss+'x'; 
		end;

		if (a1[i]=1) and (a2[i]=1) then begin s:=s+'x'; end;
		if (a1[i]>1) and (a2[i]>1) and (i<>1) then begin 
			str(a1[i],ss); 
			s:=s+'+'+ss+'x^'; 
			str(a2[i],ss); 
			s:=s+ss; 
		end;

		if ((a1[i]<1) and (a2[i]>1)) or ((a1[i]>1) and (a2[i]>1) and (i=1)) then begin 
			str(a1[i],ss); 
			s:=s+ss+'x^'; 
			str(a2[i],ss); 
			s:=s+ss; 
		end;
	end;

end;

begin
	assign(t,'polymul.in');
	assign(t1,'polymul.out');
	reset(t);
	rewrite(t1);
	readln(t,s1);
	readln(t,s2);

	vvod_polinom(s1,a1,a2,ka);
	vvod_polinom(s2,b1,b2,kb);
	mnozh_polinom(a1,a2,b1,b2,ka,kb,c1,c2,kc);

	norm_polinom(c1,c2,kc,d1,d2,kd);
	vyvod_polinom(d1,d2,kd,s3);
{	writeln(ka,' ',kb);
	writeln(a1[1],' ',a2[1]);}
{	for i:=1 to ka do 
		write(a1[i],' ',a2[i],' ');

	writeln;}

{	
	for i:=1 to kc do 
		write(c1[i],' ',c2[i],' ');}

	for i:=1 to kd do 
		write(d1[i],' ',d2[i],' ');
	
	writeln;


	writeln(s3);

	close(t);
	close(t1);
end.
