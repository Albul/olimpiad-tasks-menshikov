const 
	wall=-2;
	free=-1;
	put=-3;
label kin;
var 
	t,t1:text;
	n,i,j,k,di,dj,c:integer;
	a,a2:array[0..41,0..41] of char;
	a1:array[0..41,0..41] of integer;

procedure pp(i,j:INteger);
	var t,d,k:integer;
	tt:array[1..4] of integer;
begin
	if a1[i,j]<>0 then begin
		t:=a1[i,j];
		a1[i,j]:=put;
		tt[1]:=t-a1[i-1,j];
		tt[2]:=t-a1[i+1,j];
		tt[3]:=t-a1[i,j-1];
		tt[4]:=t-a1[i,j+1];

		for k:=1 to 4 do
			if tt[k]=1 then d:=k;

		case d of
		1: pp(i-1,j);
		2: pp(i+1,j);
		3: pp(i,j-1);
		4: pp(i,j+1);
		end;


	end;
end;


begin
	assign(t,'lines1.in');
	assign(t1,'lines.out');
	reset(t);
	rewrite(t1);

	readln(t,n);
	
	for i:=0 to n+1 do
		for j:=0 to n+1 do
			a1[i,j]:=wall;

	for i:=1 to n do
		for j:=1 to n do
			if j=n then readln(t,a[i,j])
				else read(t,a[i,j]);
				
	for i:=1 to n do
		for j:=1 to n do begin
			if a[i,j]='X' then begin 
				a1[i,j]:=free;
				di:=i;
				dj:=j; 
			end;
			if a[i,j]='O' then a1[i,j]:=wall;
			if a[i,j]='@' then a1[i,j]:=0;
			if a[i,j]='.' then a1[i,j]:=free;
		end;





	c:=0;

	while (a1[di,dj]<0) and (c<40*40) do begin
		for i:=1 to n do
			for j:=1 to n do
				if ((a1[i-1,j]=c) or (a1[i+1,j]=c) or (a1[i,j-1]=c) or (a1[i,j+1]=c)) and (a1[i,j]=-1) then begin a1[i,j]:=c+1; writeln(a1[i,j]); end;
		c:=c+1;

	end;
	
	if a1[di,dj]=free then begin 
		writeln(t1,'N'); 
		goto kin;

	end;
	

	pp(di,dj);

	writeln(t1,'Y');

	for i:=1 to n do begin
		for j:=1 to n do begin
			if a1[i,j]>0 then write(t1,'.');
			if a1[i,j]=put then write(t1,'+');
			if a1[i,j]=wall then write(t1,'#');
			if a1[i,j]=0 then write(t1,'@');
		end;
		writeln(t1);
	end;

	kin:close(t);
	close(t1);
end.
