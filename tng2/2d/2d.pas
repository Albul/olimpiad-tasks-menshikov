label kin;
var
	x1,x2,x3,x4,y1,y2,y3,y4:integer;
	a,b,c,p1,p2:extended;
	t,t1:text;
begin
	assign(t,'segments.in');
	assign(t1,'segments.out');
	reset(t);
	rewrite(t1);
	
	readln(t,x1,y1);
	readln(t,x2,y2);
	readln(t,x3,y3);
	readln(t,x4,y4);
	
	a:=0;
	b:=0;

	p1:=x4*(y2-y1)-y4*(x2-x1)+y1*x2-x1*y2;
	p2:=x3*(y2-y1)-y3*(x2-x1)+y1*x2-x1*y2;


	if (p1=0) or (p2=0) then begin 
		write(t1,'Yes');
		goto kin;
	end;
	if p1*p2<=0 then a:=1;

	p1:=x1*(y4-y3)-y1*(x4-x3)+y3*x4-x3*y4;
	p2:=x2*(y4-y3)-y2*(x4-x3)+y3*x4-x3*y4;

	if (p1=0) or (p2=0) then begin 
		write(t1,'Yes');
		goto kin;
	end;


	if p1*p2<=0 then b:=1;

	if (a=1) and (b=1) then write(t1,'Yes')
		else write(t1,'No');

	kin:close(t);
	close(t1);
end.
